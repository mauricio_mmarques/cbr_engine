from setuptools import setup

setup(
    name='cbr_engine',
    version='0.9',
    author="Maurício Martinez Marques",
    include_package_data=True,
    packages=['cbr_engine'],
    platforms=['any'],
    install_requires=[
        "pandas",
        "scipy",
        "sklearn"
    ]
)
